﻿using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swpublished;
using System.Runtime.InteropServices;
using System.Reflection;
using System;

namespace HookIntoCommandExample
{
    [Guid("9685d51e-54f0-4dc5-9d21-cf70b5304850"), ComVisible(true)]
    public class Main : SwAddin
    {

        internal static SldWorks App { get; private set; }

        internal EventHandler myEventHandler;

        public bool ConnectToSW(object ThisSW, int Cookie)
        {
            App = (SldWorks)ThisSW;
            myEventHandler = new EventHandler();
            myEventHandler.AttachEventHandlers();
            return true;
        }

        public bool DisconnectFromSW()
        {
            myEventHandler.RemoveEventHandlers();
            Marshal.ReleaseComObject(App);
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            return true;
        }

        [ComRegisterFunction()]
        private static void RegisterFunction(Type t)
        {
            Microsoft.Win32.RegistryKey hklm = Microsoft.Win32.Registry.LocalMachine;
            Microsoft.Win32.RegistryKey hkcu = Microsoft.Win32.Registry.CurrentUser;
            string keyname = "SOFTWARE\\SolidWorks\\Addins\\{" + t.GUID.ToString() + "}";
            Microsoft.Win32.RegistryKey addinkey = hklm.CreateSubKey(keyname);
            addinkey.SetValue(null, 1);
            addinkey.SetValue("Description", Assembly.GetExecutingAssembly().GetName().Name);
            addinkey.SetValue("Title", Assembly.GetExecutingAssembly().GetName().Name);
            keyname = "Software\\SolidWorks\\AddInsStartup\\{" + t.GUID.ToString() + "}";
            addinkey = hkcu.CreateSubKey(keyname);
            addinkey.SetValue(null, 1);
        }

        [ComUnregisterFunction]
        private static void UnregisterFunction(Type t)
        {
            Microsoft.Win32.RegistryKey hkcu = Microsoft.Win32.Registry.CurrentUser;
            string keyname = "SOFTWARE\\SolidWorks\\AddinsStartup\\{" + t.GUID.ToString() + "}";
            hkcu.DeleteSubKey(keyname);
            Microsoft.Win32.RegistryKey hklm = Microsoft.Win32.Registry.LocalMachine;
            keyname = "SOFTWARE\\SolidWorks\\Addins\\{" + t.GUID.ToString() + "}";
            hklm.DeleteSubKey(keyname);
        }
    }
}
