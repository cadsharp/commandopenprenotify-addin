﻿using SolidWorks.Interop.swcommands;

namespace HookIntoCommandExample
{
    class EventHandler
    {
        internal void AttachEventHandlers()
        {
            Main.App.CommandOpenPreNotify += App_CommandOpenPreNotify;
        }

        internal void RemoveEventHandlers()
        {
            Main.App.CommandOpenPreNotify -= App_CommandOpenPreNotify;
        }

        private int App_CommandOpenPreNotify(int command, int userCommand)
        {
            // This is where we test to see if a command was issued,
            // such as opening the custom properties dialog.
            // If it was, we want to run our custom action and then cancel
            // the normal action by returning 1. Otherwise, we allow the
            // default behavior to occur by returning 0.
            if (command == (int)swCommands_e.swCommands_File_Summaryinfo)
            {
                Main.App.SendMsgToUser("The Custom Properties dialog has been disabled.");
                return 1;
            }
            else
                return 0;
        }
    }
}
