## Overview ##

This addin demonstrates how to hook into or replace an existing SolidWorks command. For example, if you wanted to replace the normal File-->Save action with an action that not only saves a model but, if it is a drawing, exports a PDF to a certain directory.

In this addin, the Statistics / Performance Evaluation dialog is replaced with a message box.

This occurs using the OpenCommandPreNotify event, which is a "member" of ISldWorks. To replace a certain command, that command must have a corresponding enumerator in swCommands_e.

## Setup ##

First, this addin uses these third party references, all of which are found in the SolidWorks installation folder under \api\redist:

* SolidWorks.Interop.sldworks.dll (contains the Solidworks API)
* SolidWorks.Interop.swpublished.dll (contains ISwAddin, which is used for creating addins)
* SolidWorks.Interop.swcommands.dll (contains the swCommands_e enumeration)

You will need to re-add these references since these libraries are not under source control.

Second, you will need to register the addin. You can do this easily by running the register.bat file in the project folder. Use unregister.bat to unregister the addin.